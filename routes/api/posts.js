const express = require('express');
const router = express.Router();

// Post Model
const Posts = require('../../models/Posts')

// @routes GET api/posts
// GET ALL post
router.get('/', async (req,res) => {
    try {
        const posts = await Posts.find();
        if(!posts) throw Error('No Items');
        res.status(200).json(posts);
    } catch (err) {
        res.status(400).json({ msg: err })
    }
});

// @routes POST api/posts
// Create a post
router.post('/', async (req, res) => {
    // res.send('Anda sedang membuat postingan');
    // console.log(req.body);
    const newPost = new Posts(req.body);
    try {
        const post = await newPost.save();
        if(!post) throw Error("Something went wrong");
        res.status(200).json(post);
    } catch (err) {
        res.status(400).json({ msg: err })
    }
});

// @routes DELETE api/posts/:id
// Delete a post
router.delete('/:id', async (req, res) => {
        try {
            const post = await Posts.findByIdAndDelete(req.params.id);
            res.status(200).json({ success: true });
        } catch (err) {
            res.status(500).json({
            message: err.message,
        });
        }
});

// @routes DELETE api/posts/:id
// Delete a post
router.patch('/:id', async (req, res) => {
    try {
        const post = await Posts.findByIdAndUpdate(req.params.id, req.body);
        if(!post) throw Error("Something went wrong while update");
        res.status(200).json({ success: true });
    } catch (err) {
        res.status(400).json({ msg: err });
    }
})

// @routes GET api/posts/:id
// Get a specific post
router.get('/:id', async (req,res) => {
    try {
        const post = await Posts.findById(req.params.id);
        if(!post) throw Error('No Items');
        res.status(200).json(post);
    } catch (err) {
        res.status(400).json({ msg: err })
    }
});

module.exports = router;