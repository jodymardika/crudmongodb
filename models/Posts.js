const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var collectionName = 'UmrohCatalog';

const PostSchema = new Schema({
    vendor: {
        id: {
            type: String,
            required: true
        },
        vendor_name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            required: true
        },
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        requires: true
    },
    slug: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quota: {
        type: Number,
        required: true
    },
    paylater: {
        type: Boolean,
        required: true
    },
    cashback: {
        jamaah: {
            model: {
                type: String,
                required: true
            },
            nominal: {
                type: Number,
                required: true
            }
        },
        agent_standard: {
            model: {
                type: String,
                required: true
            },
            nominal: {
                type: Number,
                required: true
            }
        },
        agent_vip: {
            model: {
                type: String,
                required: true
            },
            nominal: {
                type: Number,
                required: true
            }
        }
    },
    schedule_umroh: {
        manasik_date: {
            type: Date,
            required: true
        },
        takeoff_date: {
            type: Date,
            required: true
        },
        home_date: {
            type: Date,
            required: true
        }
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
});

module.exports = mongoose.model(collectionName, PostSchema);