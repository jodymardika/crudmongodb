const express = require('express');
const mongoose = require('mongoose');
const { MONGO_URI } = require('./config');

// Routes
const postsRoutes = require('./routes/api/posts');

const app = express();

// BodyParser Middleware
app.use(express.json());

// koneksi ke mongoDB
mongoose.connect(MONGO_URI,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => console.log('MongoDB berhasil terkoneksi!'))
.catch(err => console.log(err));

// User routes
app.use('/api/posts', postsRoutes)

// app.get('/', (req, res) => {
//     res.send(`Hello World`);
// })

const PORT = process.env.PORT || 5000

app.listen(PORT, () => console.log(`Server berjalan di port ${PORT}`));