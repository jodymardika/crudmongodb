const  MongoClient  = require('mongodb').MongoClient;

async function main() {
    const uri = "mongodb+srv://jodymardika:12345vendeta@cluster0.comgt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true }); //instance of mongo client

    try {
        await client.connect(); //block further instruction until that operation complete
        // await listDatabases(client); //menampilkan db yang tersedia
        // await createlisting(client, {
        //     name: "Lovely Loft 2",
        //     summary: "A charming loft in paris 2",
        //     bedrooms: 1,
        //     bathroom: 1
        // });
        // await  createMultipleListing(client, [
        //     {
        //         name: "Lovely Loft 3",
        //         summary: "A charming loft in paris 3",
        //         bedrooms: 1,
        //         bathroom: 1
        //     },
        //     {
        //         name: "Lovely Loft 4",
        //         summary: "A charming loft in paris 4",
        //         bedrooms: 1,
        //         bathroom: 1
        //     },
        //     {
        //         name: "Lovely Loft 5",
        //         summary: "A charming loft in paris 5",
        //         bedrooms: 2,
        //         bathroom: 2
        //     },
        //     {
        //         name: "Lovely Loft 6",
        //         summary: "A charming loft in paris 6",
        //         bedrooms: 2,
        //         bathroom: 2
        //     }
        // ]);
        // await findOneListingByName (client, "Lovely Loft 5");
        // await findListingWithMinimumBedromsBathroomsAndMostRecentReviews(client, {
        //     minimumNumberofBedrooms: 2,
        //     minimumNumberofBathroom: 2,
        //     maximumNumberofResult: 5
        // });

    } catch (error) {
        console.error(error);
    } finally{
        await client.close() //close connection
    }
}

main().catch(console.error);

async function createlisting(client, newListing){
    const result = await client.db("sample_airbnb").collection("ListingAndReviews").insertOne(newListing);

    console.log(`New Listing created with the folowing id: ${result.insertedId}`);
}

async function createMultipleListing(client, newListing) {
    const result = await client.db("sample_airbnb").collection("ListingAndReviews").insertMany(newListing);

    console.log(`${result.insertedCount} new listing created wit the flowing id(s):`);
    console.log(result.insertedIds)
}

async function findOneListingByName(client, nameOfListing) {
    const result = await client.db("sample_airbnb").collection("ListingAndReviews").findOne({ name: nameOfListing });

    if (result) {
        console.log(`Found a listing in the collection with the name '${nameOfListing}'`);
        console.log(result);
    }else {
        console.log(`No Listing found with the name '${nameOfListing}'`);
    }
}

async function findListingWithMinimumBedromsBathroomsAndMostRecentReviews(client, {
    minimumNumberofBedrooms = 0,
    minimumNumberofBathroom = 0,
    maximumNumberofResult =  Number.MAX_SAFE_INTEGER
} = {}) {
    const cursor = await client.db("sample_airbnb").collection("ListingAndReviews").find({
        bedrooms: { $gte: minimumNumberofBedrooms},
        bathroom: {$gte: minimumNumberofBathroom}
    }).sort({ last_review: -1}).limit(maximumNumberofResult);

    const result = await cursor.toArray();
    console.log(result);
}
async function listDatabases(client) {
    const databaseList = await client.db().admin().listDatabases(); //cobalist db

    console.log("Databases:")
    databaseList.databases.forEach(db => {
        console.log(`- ${db.name}`);
    });
}
// tes 3
